import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import authSlice from '../shared/auth-slice';
import { produitApi } from '../shared/Product-api';
import { userApi } from '../shared/User-api';



export const store = configureStore({
    reducer: {
        [userApi.reducerPath]: userApi.reducer,
        [produitApi.reducerPath]: produitApi.reducer,
        auth:authSlice
    },

    middleware: (getDefaultMiddleware ) =>
        getDefaultMiddleware().concat(userApi.middleware, produitApi.middleware)

});


export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
    ReturnType,
    RootState,
    unknown,
    Action<string>
>;