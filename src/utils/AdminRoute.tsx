import { Redirect, Route, RouteProps } from "react-router-dom";
import { useAppSelector } from "../stores/hooks";

export function AdminRoute({ children, ...rest }: RouteProps) {
    const user = useAppSelector(state => state.auth.user)
    return (
        <Route {...rest}
            render={({ location }) =>
                user && user.role === 'admin' ? (
                    children
                ) : (
                    <Redirect to={{
                        pathname: '/login',
                        state: { from: location },
                    }
                    }
                    />
                )
            }
        />
    )
}