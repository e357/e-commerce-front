import { Redirect, Route, RouteProps } from "react-router-dom";
import { useAppSelector } from "../stores/hooks";

export function PrivateRoute({ children, ...rest }: RouteProps) {
    const user = useAppSelector(state => state.auth.user)
    return (
        <Route {...rest}
            render={({ location }) =>
                user ? (
                    children
                ) : (
                    <Redirect to={{
                        pathname: '/login',
                        state: { from: location },
                    }
                    }
                    />
                )
            }
        />
    )
}