import { LegacyRef, useRef, useState } from "react";
import { Produit } from "../Entities";
import {  useUpdateProductMutation } from "../shared/Product-api";


interface Props {
    produit: Produit;
}

export function ProductAdminList({ produit }: Props) {
    const [editProduct] = useUpdateProductMutation();
    const [form, setForm] = useState<Produit>({} as Produit);
    const formref: LegacyRef<any> = useRef()

    const handleFile = async (event: React.FormEvent<EventTarget>) => {
        let target = event.target as HTMLInputElement;
        let name = target.name;
        let value = target.files?.item(0);
        let change = { ...form, [name]: value }

        setForm(change)
    }


    const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
        try {
            event.preventDefault()

            const formData = new FormData(formref.current);


            const send = await editProduct({body:formData, id:produit.id}).unwrap()


        } catch (error: any) {
            console.log(error.data);
        }
    }

    const handleChange = (event: React.FormEvent<EventTarget>) => {
        let target = event.target as HTMLInputElement;
        let name = target.name;
        let value = target.value
        let change = { ...form, [name]: value }

        setForm(change)
    }
    

    
    return (
        <>
            <tbody>


                <tr className="table-secondary table-responsive" >

                    <td>{produit.id}</td>
                    <td>

                        <img src={produit.thumbnailPic} height="150px" alt="" />

                    </td>
                    <td>{produit.name}</td>
                    <td>{produit.category}</td>
                    <td>{produit.price} €</td>
                    {produit.active ?
                        <td><span className="badge rounded-pill bg-success">Available</span></td>
                        :
                        <td> <span className="badge rounded-pill bg-danger">Unavailable</span></td>
                    }
                    <td>{produit.description}</td>
                    <td><button data-bs-toggle="modal" data-bs-target={"#editModal" + produit.id} className="btn btn-outline-info mx-1"><i className="bi bi-pencil-square"></i></button></td>
                </tr>

            </tbody>


            <div className="modal fade" id={"editModal" + produit.id} tabIndex={-1} aria-labelledby="editModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content bg-light">
                        <div className="modal-header">
                            <h5 className="modal-title" id="editModalLabel">Modal title</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>

                        <div className="modal-body">
                            <form ref={formref} onSubmit={handleSubmit} >
                                <div className="form-group row">
                                    <label htmlFor="text" className="col-sm-2 col-form-label">Name</label>
                                    <input defaultValue={produit.name} onChange={handleChange} name="name" type="text" className="form-control border-2" />
                                    <label htmlFor="text" className="col-sm-2 col-form-label">Category</label>
                                    <input defaultValue={produit.category} onChange={handleChange} name="category" type="text" className="form-control border-2" />
                                    <label htmlFor="text" className="col-sm-2 col-form-label">Price</label>
                                    <input defaultValue={produit.price} onChange={handleChange} name="price" type="text" className="form-control border-2" />


                                    <label htmlFor="floatingTextarea" className="mt-3">Description</label>
                                    <textarea defaultValue={produit.description} onChange={handleChange} name="description" className="form-control" placeholder="Describe your product" id="floatingTextarea"></textarea>


                                    <div className="form-group">
                                        <label htmlFor="formFile" className="form-label mt-4">Photo</label>
                                        <input onChange={handleFile} name="picture" className="form-control" type="file" id="formFile" />
                                    </div>
                                    {/* <label className="form-check-label col-sm-2 col-form-label mt-3" htmlFor="flexSwitchCheckDefault">Stock</label> */}
                                    {/* <div className="form-check form-switch mx-3 ">
                    
                    <input className="form-check-input" type="checkbox" id="flexSwitchCheckDefault" />
                  </div> */}
                                    <label htmlFor="text" className="col-sm-2 col-form-label">Stock</label>
                                    <input defaultValue={produit.stock} onChange={handleChange} name="stock" type="text" className="form-control border-2" />
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                    <button type="submit" className="btn btn-primary">Edit product</button>
                                </div>
                            </form>
                        </div>


                    </div>
                </div>
            </div>

        </>



    )
}
// Ajouter des petites photos
// Faire une fonction pour le stock si available = pill verte si out of stock = pill rouge
// Ajouter fonction edit et delete