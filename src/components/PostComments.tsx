import { useState } from "react"
import { Comments, Produit } from "../Entities"
import { useAddCommentProductMutation } from "../shared/Product-api"




interface Props{
    product: Produit
}
export function PostComments({product}:Props) {


    const [postComment] = useAddCommentProductMutation()
    const [form, setForm] = useState<Comments>({} as Comments)


    const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
        try {
            event.preventDefault()
            await postComment({...form, produit: product, date: new Date()}).unwrap()
            
        } catch  (error:any) {
            
        }

    }

    const handleChange = (event: React.FormEvent<EventTarget>) => {

        let target = event.target as HTMLInputElement;
        let name = target.name
        let value = target.value
        let change = { ...form, [name]: value }

        setForm(change)

    }
    return (
        <>
            <form onSubmit={handleSubmit}>
                <div className="mb-3 d-flex flex-column">
                    <label htmlFor="exampleFormControlTextarea1" className="form-label">Ajouter un commentaire</label>
                    <textarea onChange={handleChange} className="form-control border border-primary" name="text" id="exampleFormControlTextarea1" ></textarea>
                    <button type="submit" className='btn btn-sm btn-primary mt-1 w-25' >Submit</button>
                </div>
            </form>
        </>
    )
}