import { Link } from "react-router-dom";
import { Produit } from "../Entities";
import { useAppSelector } from "../stores/hooks";
import {useHistory} from 'react-router'
import { usePostCartMutation } from "../shared/User-api";
interface Props {
    produit: Produit;
}

export function CardProduit({ produit }: Props) {
    const user = useAppSelector(state => state.auth.user)
    let history = useHistory()
    const [postPanier] = usePostCartMutation()
    
    function AddToCart(product: Produit) {
        //get the product by id
        //it gets added to the card onClick
        if (!user) {
            return history.push('/login')
        }
        postPanier(product.id)
    
    }
    return (
        <div className="col-12">
                <div className="card">
                    <img src={produit.thumbnailPic} className="card-img-top" alt={produit.name} />
                    <div className="card-body">
                        <h5 className="card-title">{produit.name}</h5>
                        <h6 className="card-subtitle mb-2">{produit.price}€</h6>
                    </div>
                </div>
                    <Link to={'/product/' + produit.id} className='btn btn-sm btn-primary col-6'>Voir le produit</Link>
                    <button onClick={()=>AddToCart(produit)} className='btn btn-sm btn-secondary col-6'>Ajouter au panier</button>
        </div>
    )
}