import { Link } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../stores/hooks";
import Avatar from 'react-avatar'
import { logout } from "../shared/auth-slice";
import { useState } from "react";
import {useHistory} from 'react-router';
export function Nav() {
    const [search, setSearch] = useState<string | null>()
    const user = useAppSelector(state => state.auth.user)
    let dispatch = useAppDispatch();
    let history = useHistory()

    const handleSearch = async (event: React.FormEvent<EventTarget>) => {
        let target = event.target as HTMLInputElement
        setSearch(target.value)
    }   

    const handleSearchSubmit = (event: React.FormEvent<EventTarget>) => {
        event.preventDefault();
        history.push('/search/'+search)

    }
    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
                <div className="container-fluid">
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarColor01">
                        <ul className="navbar-nav me-auto">
                            <li className="nav-item">
                                <Link className="nav-link text-white" to="/">Home</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link text-white" to='/products'>Produits</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link text-white" to="/cart">Panier <i className="bi bi-cart"></i>{user?.panier?.length ? <span className='badge rounded bg-success ms-1'>{user?.panier?.length}</span> : null}</Link>
                            </li>

                        </ul>
                        <ul className="navbar-nav">
                            <form className="input-group nav-item" onSubmit={handleSearchSubmit}>
                                <input onChange={handleSearch} className="form-control me-sm-1 searchbar" type="text" placeholder="Search"></input>
                                <button className="btn btn-sm btn-secondary" type="submit">Search</button>
                            </form>
                        </ul>

                        <ul className="navbar-nav">
                            {!user ? <>
                                <li className="nav-item">
                                    <Link className="nav-link text-white" to="/login">Login</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link text-white" to="/register">Register</Link>
                                </li>
                            </>
                                :
                                <>
                                    <li className="nav-item">
                                        <div className="btn-group">
                                            <button className="btn-link btn btn-sm text-white dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                <span className='fs-5'>{user.name}</span>
                                                <Avatar style={{ marginLeft: 10 }} round name={user.name} src={user.avatar} size="35px"></Avatar>
                                            </button>
                                            <ul className="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton1">
                                                <li><Link className="dropdown-item" to="/account">Compte</Link></li>
                                                <li><button onClick={() => dispatch(logout())} className="dropdown-item" >Déconnexion</button></li>
                                            </ul>
                                        </div>
                                    </li>
                                </>
                            }

                        </ul>
                    </div>

                </div>
            </nav>
        </div>

    )
}