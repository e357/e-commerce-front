import { useState } from "react"
import Avatar from "react-avatar"
import { Redirect } from "react-router"
import { useCreateCommandMutation } from "../shared/User-api"
import { useAppSelector } from "../stores/hooks"

export const Order = () => {
    const [next, setNext] = useState(false)
    const user = useAppSelector(state => state.auth.user)
    const [postOrder, responseOrder] = useCreateCommandMutation()
    const [done, setDone] = useState(false)
    async function createOrder(){
        if (user?.id) {
            await postOrder(Number(user?.id)).unwrap();
            if (!responseOrder.isError) {
                setDone(true)
            }
        }

    }

    if (done) {
        return <Redirect to='/'/>
    }


    let total = 0;
    return !next ? <>
        <h4 className=" text-center">Choisir l'adresse de livraison : </h4>
        <div className='bg-light rounded mt-1 p-3'>
            <form className='row align-items-center flex-column'>
                <div className="form-group col-8">
                    <label htmlFor=''>Nom complet</label>
                    <input type="text" required defaultValue={user?.name} className="form-control" name='name' placeholder='John Smith' />
                </div>
                <div className="form-group col-8 mt-2">
                    <label htmlFor=''>Adresse</label>
                    <input min={20} type="text" required defaultValue={user?.address} className="form-control" name='address' placeholder='34 rue Antoine Primat 69100 Villeurbanne' />
                </div>
                <button type='submit' className='btn btn-primary btn-sm col-4 mt-4' onClick={() => setNext(true)}>Continuer la commande</button>
            </form>
        </div>

    </>
        :
        <>
            <h4 className=" text-center">Finalisation de la commande</h4>
            <div className='bg-light rounded mt-1 p-3'>
                {user?.panier?.map(item => {
                    total += (item.price * item.quantity);
                    return <div className='row align-items-center justify-content-around m-1'>
                        <div className='col-2 text-center align-items-center d-flex flex-column mt-1'>
                            <small>{item.produit.name}</small>
                            <Avatar size='50' src={item.produit.thumbnailPic} name={item.produit.name} />
                        </div>
                        <div className='col-2 text-center d-flex flex-column'>
                            <h6>Quantité</h6>
                            <p>x{item.quantity}</p>
                        </div>
                        <div className='col-3 text-center d-flex flex-column'>
                            <h6>Prix unitaire</h6>
                            <p>{item.price}€</p>
                        </div>
                        <hr className='mt-2' />

                    </div>
                })}
                <div className='col-12 text-end d-flex align-items-end justify-content-end flex-column'>
                    <h6>Total</h6>
                    <small>{total}€</small>
                    <button className='btn btn-primary col-3 mt-2' onClick={()=>createOrder}>Commander</button>
                </div>
            </div>
        </>
}