import { useState } from "react";
import { Redirect } from "react-router";
import { User } from "../Entities";
import { setCredentials } from "../shared/auth-slice";
import { useUserLoginMutation } from "../shared/User-api";
import { useAppDispatch, useAppSelector } from "../stores/hooks";

export function LoginForm() {
    let user = useAppSelector(state => state.auth.user)

    const [postLogin, postQuery] = useUserLoginMutation();
    const [form, setForm] = useState<User>({} as User);
    let dispatch = useAppDispatch()
    /**
     * Post the form to the server
     * @param event 
     */
    const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
        try {
            event.preventDefault();
        const data = await postLogin(form).unwrap()
        if (data && !postQuery.isError) {
            dispatch(setCredentials(data))
            
        }
        } catch (error:any) {
            console.log(error.data);
            
        }
        
    }

    const handleChange = (event: React.FormEvent<EventTarget>) => {
        let target = event.target as HTMLInputElement;
        let name = target.name;
        let value = target.value
        let change = { ...form, [name]: value }


        setForm(change)
    }
    return (
        !user ? <div className="container mt-5">
            <div className="d-flex justify-content-center ">
                <div className="row">
                    <div className="col-12">
                        <form onSubmit={handleSubmit} className="shadow p-5 my-4  rounded bg-white">
                            <div className="row mb-3">
                                <label htmlFor="inputEmail3" className="col-sm-3 col-form-label">Email</label>
                                <div className="col-sm-12 col-md-8 ">
                                    <input onChange={handleChange} name='email' type="email" className="form-control border-2" id="inputEmail3" />
                                </div>
                            </div>
                            <div className="row mb-3">
                                <label htmlFor="inputPassword3" className="col-sm-3 col-form-label">Password</label>
                                <div className="col-sm-12 col-md-8 ">
                                    <input onChange={handleChange} name='password' type="password" className="form-control border-2" id="inputPassword3" />
                                </div>
                            </div>
                            <div className="d-flex justify-content-center pt-3">
                                <button type="submit" className="btn btn-primary ">Login</button>
                            </div>
                        </form>
                        {postQuery.isError && <h5 className='text-center text-uppercase text-white bg-danger py-2 m-1' >{(postQuery.error as any).data.error}</h5>}
                    </div>
                </div>
            </div>
        </div> : <Redirect to='/' />


    )
}
