import React, { useState } from "react";
import { User } from "../Entities";
import {useUserUpdateMutation } from "../shared/User-api";
import { useAppSelector } from "../stores/hooks";
interface Props {
    edit: Function
    dataEdit: Boolean
}
export function AccountForm({ edit, dataEdit }: Props) {
    let user = useAppSelector(state => state.auth.user);

    const [form, setForm] = useState<User | null>(null)
    const [updateUser] = useUserUpdateMutation();


    const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
        event.preventDefault();
        if (form !== null) {
            await updateUser(form).unwrap()
            

        }
    }

    const handleChange = (event: React.FormEvent<EventTarget>) => {
        let target = event.target as HTMLInputElement;
        let name = target.name;
        let value = target.value
        let change = { ...user, [name]: value }
        console.log(change);
        

        setForm(change)
    }


    return (
        <>
            
            {dataEdit ? <> <fieldset id="account">
                <label className="form-label mt-4" htmlFor="readOnlyInput">Name</label>
                <input onChange={handleChange} name="name" className="form-control" type="text" defaultValue={user?.name} />
            </fieldset>
                <fieldset>
                    <label className="form-label mt-4" htmlFor="readOnlyInput">Email</label>
                    <input onChange={handleChange} name="email" className="form-control" type="text" defaultValue={user?.email} />
                </fieldset>

                <fieldset>
                    <label className="form-label mt-4" htmlFor="readOnlyInput">Address</label>
                    <input  onChange={handleChange} name="address" className="form-control" type="text" defaultValue={user?.address} placeholder="address" />
                </fieldset>
                <fieldset>
                    <label className="form-label mt-4" htmlFor="readOnlyInput">Phone</label>
                    <input onChange={handleChange} name="phone" className="form-control" type="text" defaultValue={user?.phone} />
                </fieldset></>


                :

                <>
                    <p>Name : {user?.name}</p>
                    <p>Email: {user?.email}</p>
                    <p>Address: {user?.address}</p>
                    <p>Phone: {user?.phone}</p>
                </>}





            { !dataEdit ?
                <div className="d-flex justify-content-center pt-3 mt-3">
                    <button onClick={() => edit(true)} type="button" className="btn btn-outline-primary">Modifier</button>
                </div>

                : 

                <div className="d-flex justify-content-center pt-3 mt-3">
                    <button onClick={handleSubmit} className="btn btn-outline-primary">Submit</button>
                </div>
                
            }
        
        

        </>
    )
}