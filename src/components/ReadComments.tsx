import Avatar from "react-avatar"
import { Comments } from "../Entities"
import moment from 'moment'

interface Props {
    comments: Comments[]
}

export const Commentaires = ({ comments }: Props) => {

    return (
        <>
            {comments.map((value, index) => {
                return <div key={value.id}>
                <div className='d-flex align-items-center '>
                    <Avatar  size='40' round name={value.user.name}  color='black' src={value.user.avatar} />
                    <span className='ms-3'>{moment(value.date).format('ll')}</span>
                </div>
                <hr></hr>
                <div className='w-100 bg-comments text-black rounded px-3 py-2 mb-3'>{value.text}</div>

                </div>
            })}
        </>
    )
}