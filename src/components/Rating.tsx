import React, { useState } from 'react';
import { FaStar } from "react-icons/fa";
import "./Rating.css"

export function Rating () {

    const [rating, setRating] = useState<number>(0);
    const [hover, setHover] = useState<number>(0);


    return (
        <div className="mt-3">

            {[...Array(5)].map((star, i) => {

                const ratingValue = i + 1;

            return (

            <label>
                <input type="radio" name="rating" value={ratingValue}
                onClick={() => setRating(ratingValue)}
                />
                <FaStar className="star" 
                color={ratingValue <= (hover || rating) ? '#ffc107' : '#e4e5e9'}
                size={30}
                onMouseOver = {() => setHover(ratingValue)}
                onMouseLeave = {() => setHover(0)}
                />
            </label>
            );
        })}
         <p className="mt-3">Rating: {rating}</p>
        </div>
    )
}

