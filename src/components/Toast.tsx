interface Props{
    sentence: string
}

export function Toast({sentence}:Props) {
    return (
        <div className="alert alert-dismissible alert-success">
            <button type="button" className="btn-close" data-bs-dismiss="alert"></button>
            <strong className='text-center'>{sentence}</strong>
        </div>
    )
}