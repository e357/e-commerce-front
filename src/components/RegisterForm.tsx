import React, { LegacyRef, useRef, useState } from "react";
import { Redirect } from "react-router";
import { User } from "../Entities";
import { setCredentials } from "../shared/auth-slice";
import { useUserRegisterMutation } from "../shared/User-api";
import { useAppDispatch, useAppSelector } from "../stores/hooks";

export function RegisterForm() {

    let user = useAppSelector(state => state.auth.user)

    const [postRegister, postQuery] = useUserRegisterMutation();
    const [form, setForm] = useState<User>({} as User);
    let dispatch = useAppDispatch()
    
const formref:LegacyRef<any>= useRef()

    const handleFile = async (event: React.FormEvent<EventTarget>)=>{
        let target = event.target as HTMLInputElement;
        let name = target.name;
        let value = target.files?.item(0);
        let change = { ...form, [name]: value }

        setForm(change)
    }


    const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
        try {
            event.preventDefault()

        const formData = new FormData(formref.current);
        // formData.append('name', form.name!);
        // formData.append('email', form.email!);
        // formData.append('password', form.password!);
        // formData.append('address', form.address!);
        // formData.append('phone', form.phone!);
        // formData.append('avatar', form.avatar!);
        
            
            const send = await postRegister(formData).unwrap()
    
            if (send.token) {
                dispatch(setCredentials(send))

            }
        } catch (error: any) {
            console.log(error.data);
        }
    }

    const handleChange = (event: React.FormEvent<EventTarget>) => {
        let target = event.target as HTMLInputElement;
        let name = target.name;
        let value = target.value
        let change = { ...form, [name]: value }

        setForm(change)
    }

    return (
        !user ? <div className="container mt-3">
            <div className="d-flex justify-content-center ">
                <div className="row">
                    <div className="col-12">

                        <form ref={formref} onSubmit={handleSubmit} className="shadow px-5 py-3 my-4 bg-white rounded">
                            {/* name and password */}
                            <div className="row">

                                <div className="col-md-6 ">
                                    <div className="input-group input-group-sm mb-3">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text" id="inputGroup-sizing-sm">Name*</span>
                                        </div>
                                        <input onChange={handleChange} name="name" type="text" id="inputEmail3" className="form-control border-2" aria-label="Small" aria-describedby="inputGroup-sizing-sm" />
                                    </div>
                                </div>

                                <div className="col-md-6 ">
                                    <div className="input-group input-group-sm mb-3">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text" id="inputGroup-sizing-sm">Password*</span>
                                        </div>
                                        <input onChange={handleChange} name="password" type="password" className="form-control border-2" id="inputPassword3" aria-label="Small" aria-describedby="inputGroup-sizing-sm" />
                                    </div>
                                </div>
                            </div>

                            {/* email and address */}
                            <div className="row">

                                <div className="col-md-6 mb-2">
                                    <div className="input-group input-group-sm mb-3">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text" id="inputGroup-sizing-sm">Email*</span>
                                        </div>
                                        <input onChange={handleChange} name="email" type="email" className="form-control border-2" id="inputEmail3" aria-label="Small" aria-describedby="inputGroup-sizing-sm" />
                                    </div>
                                </div>

                                <div className="col-md-6 mb-2">
                                    <div className="input-group input-group-sm mb-3">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text" id="inputGroup-sizing-sm">Address</span>
                                        </div>
                                        <input onChange={handleChange} name="address" type="text" className="form-control border-2" id="inputAddress3" aria-label="Small" aria-describedby="inputGroup-sizing-sm" />
                                    </div>
                                </div>
                            </div>

                            <div className="row">

                                <div className="col-md-6 mb-2">
                                    <div className="input-group input-group-sm mb-3">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text" id="inputGroup-sizing-sm">Phone</span>
                                        </div>
                                        <input onChange={handleChange} name="phone" type="phone" className="form-control border-2" id="inputPhone3" aria-label="Small" aria-describedby="inputGroup-sizing-sm" />
                                    </div>
                                </div>

                                <div className="col-md-6  mb-2">
                                    <div className="input-group input-group-sm mb-3">
                                        <div className="form-group">
                                            <input onChange={handleFile} className="form-control" name="avatar" type="file" id="formFile" />
                                        </div>
                                    </div>
                                    {/* <div className="input-group input-group-sm mb-3">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text" id="inputGroup-sizing-sm">Image</span>
                                        </div>
                                        <input onChange={handleChange} name="image" type="image" className="form-control border-2" id="inputImage3" aria-label="Small" aria-describedby="inputGroup-sizing-sm" />
                                    </div> */}
                                </div>
                            </div>


                            <div className="d-flex justify-content-center pt-3">
                                <button type="submit" className="btn btn-primary ">Register</button>
                            </div>

                        </form>
                        {postQuery.isError && <h5 className='text-center text-uppercase text-white bg-danger py-2 m-1' >{(postQuery.error as any).data.error}</h5>}
                    </div>
                </div>
            </div>
        </div> : <Redirect to='/' />

    )
}


