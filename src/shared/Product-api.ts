import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { Comments, Produit, Rating } from '../Entities';
import { prepare } from './token';

export const produitApi = createApi({
    reducerPath: 'produitApi',
    tagTypes: ['produit', 'produitById'],
    baseQuery: fetchBaseQuery({ baseUrl: process.env.REACT_APP_SERVER_URL + '/api/produit' , prepareHeaders: prepare}),
    endpoints: (builder) => ({
        // we type query because we have a GET requeste otherwise it would be a mutation (because theres something that change while GET its just to receive some info)
        //after we say that we want - here we want the object Produit but we could say that we wanted an array or whatever the way we need to recover the info

        //after the query -> the first argument is the thing we send and the second is what we receive.
        getAllProduit: builder.query<Produit[], string | void>({
            //here is how the link will look like
            query: (parameters) =>({ 
                url:'/',
                params:{text:parameters}
            }),
            providesTags: ['produit'],
            //sometimes we need to give more info since its not a GET requeste so
            //we need to tell the method and then what we send to the back
        }),
        //providesTags -> it remembers the request on cache but sometimes the info changes
        //that's why we tell to the tag to re-do the request of the 'object' so he can actualise
        //InvalidTags-> when we make like  a POST request, we have to tell that the request we did its not good anymore
        //so we have to tell for them to make another request because its not valid anymore since the info changed


        getLast3Products: builder.query<Produit[], void>({
            query: () => '/last',
            providesTags: ['produit']
        }),


        getProduitById: builder.query<Produit, number>({

            query: (id) => ({
                url: '/' + id
            }),
            providesTags:['produitById']
        }),


        postProduct: builder.mutation<Produit, FormData>({
            query: (body) => ({
                url: '/',
                method: 'POST',
                body
            }),
            invalidatesTags: ['produit']
        }),

        deleteProduct: builder.mutation<void, Number>({
            query: (id) => ({
                url: '/' + id,
                method: 'DELETE'
            }),
            invalidatesTags: ['produit']
        }),

        updateProduct: builder.mutation<Produit, {body:FormData, id:number}>({
            query: ({body, id}) => ({
                url: '/' + id,
                method: 'PATCH',
                body
            }),
            invalidatesTags: ['produit']
        }),

        addCommentProduct: builder.mutation<Comments, Comments>({
            query: (body) => ({
                url: '/' + body.produit.id + '/comment',
                method: 'POST',
                body
            }),
            invalidatesTags: ['produitById']
        }),
        deleteCommentProduct: builder.mutation<void, Comments>({
            query: (id) => ({
                url: '/' + id + '/comment',
                method: 'DELETE'
            }),
            invalidatesTags: ['produitById']
        }),
        addRatingProduct: builder.mutation<Rating, Rating>({
            query: (body) => ({
                url: '/' + body.produit.id + '/rating',
                method: 'POST',
                body
            }),
            invalidatesTags: ['produitById']
        }),
        updateRatingProduct: builder.mutation<Rating, Rating>({
            query: (body) => ({
                url: '/' + body.produit.id + '/rating',
                method: 'PATCH',
                body
            }),
            invalidatesTags: ['produitById']
        })
    })
});


export const { useGetAllProduitQuery, useGetProduitByIdQuery, useGetLast3ProductsQuery, usePostProductMutation, useDeleteCommentProductMutation,
    useUpdateProductMutation, useAddCommentProductMutation, useDeleteProductMutation, useAddRatingProductMutation, useUpdateRatingProductMutation } = produitApi
