import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { CommandLine, User } from '../Entities';
import { AuthState } from './auth-slice';
import { prepare } from './token';

export const userApi = createApi({
    reducerPath: 'userApi',
    tagTypes: ['user'],
    baseQuery: fetchBaseQuery({ baseUrl: process.env.REACT_APP_SERVER_URL + '/api', prepareHeaders: prepare }),
    endpoints: (builder) => ({
        getThisUser: builder.query<User, void>({
            query: () => '/user/account',
            providesTags: ['user']
        }),
        userLogin: builder.mutation<AuthState, User>({
            query: (body) => ({
                url: '/user/login',
                method: 'POST',
                body
            }),
            invalidatesTags: ['user']
        }),
        userRegister: builder.mutation<AuthState, FormData>({
            query: (body) => ({
                url: '/user/register',
                method: 'POST',
                body
            })
        }),
        userUpdate: builder.mutation<User, User>({
            query: (body) => ({
                url: '/user/' + body.id,
                method: 'PATCH',
                body
            }),
            invalidatesTags: ['user']
        }),

        userDelete: builder.mutation<void, User['id']>({
            query: (id) => ({
                url: '/user/' + id,
                method: 'DELETE'
            }),
            invalidatesTags: ['user']
        }),

        userPassword: builder.mutation<User, User>({
            query: (body) => ({
                url: '/user/password/' + body.id,
                method: 'PATCH',
                body
            }),
            invalidatesTags: ['user']
        }),
        postCart: builder.mutation<CommandLine, number>({
            query: (id) => ({
                url: '/cart/'+id,
                method: 'POST'
            }),
            invalidatesTags: ['user']
        }),
        minusCart: builder.mutation<CommandLine, number>({
            query: (id) => ({
                url: '/cart/minus/'+id,
                method: 'PATCH'
            }),
            invalidatesTags: ['user']
        }),
        deleteCart: builder.mutation<void, number>({
            query: (id) => ({
                url: '/cart/'+id,
                method: 'DELETE'
            }),
            invalidatesTags: ['user']
        }),
        createCommand: builder.mutation<void, number>({
            query: (id) => ({
                url: '/'+id+'/validate',
                method: 'PATCH'
            })
        })

    })
});




export const { useGetThisUserQuery, useUserLoginMutation, useUserRegisterMutation, useUserUpdateMutation,
    useUserDeleteMutation, useUserPasswordMutation, usePostCartMutation, useMinusCartMutation, useDeleteCartMutation, useCreateCommandMutation } = userApi
