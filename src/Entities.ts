export interface User {
    id?: number,
    name?: string,
    email?: string,
    password?: string,
    address?: string,
    avatar?: string,
    phone?: string,
    panier ?: CommandLine[],
    role ?: string,
    commandes?: Commande[],
    comments?: Comments[],
    rating?: Rating[]

}

export interface CommandLine {
    id:number,
    price: number,
    quantity: number,
    produit: Produit,
    commande?: Commande,
    user?:User
}

export interface Produit {
    id:number,
    name:string,
    category:string,
    price:number,
    stock:number,
    description: string,
    picture: string,
    thumbnailPic?:string,
    rating?: Rating[],
    comments?: Comments[],
    commandLine?: CommandLine,
    active: Number
}

    
export interface Rating {
    id:number,
    note: number,
    user: User,
    produit: Produit
}

export interface Comments {
    id:number,
    date: Date,
    text:string,
    user:User,
    produit: Produit
}

export interface Commande {
    id:number,
    total:number,
    address:string,
    date:Date,
    user:User,
    line: CommandLine[]
}