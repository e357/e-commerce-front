import { useState } from "react"
import { Link, Redirect } from "react-router-dom";
import { AccountForm } from "../components/AccountForm";
import { logout, setCredentials } from "../shared/auth-slice";
import { useUserDeleteMutation, useUserPasswordMutation} from "../shared/User-api";

import { useAppDispatch, useAppSelector } from "../stores/hooks"

interface Password {
    password?: string | undefined,
    oldPassword?: string | undefined
}

export function Account() {
    const [deleteUser] = useUserDeleteMutation();
    const [passwordUpdate] = useUserPasswordMutation();
    const [edit, setEdit] = useState(false);


    let user = useAppSelector(state => state.auth.user);
    const [form, setForm] = useState<Password | null>(null)
    let dispatch = useAppDispatch()

    const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
        event.preventDefault();
        console.log(form);

        if (form !== null) {

            await passwordUpdate({
                ...form, id: user?.id

            });
            alert('Vous avez bien changé votre mot de passe')
        }

    }

    const handleChange = (event: React.FormEvent<EventTarget>) => {
        let target = event.target as HTMLInputElement;
        let name = target.name;
        let value = target.value
        let change = { ...form, [name]: value }


        setForm(change)
    }

    function delUser(id: number) {
        if (window.confirm('Voulez-vous vraiment supprimer votre compte ?')) {
            deleteUser(id)

            if (user?.id === id) {
                dispatch(setCredentials({
                    user: null,
                    token: null
                }))
            }
        }
    }



    if (user !== null) {



        return (
            <div>
                { user.role === 'admin' ?
                    <>
                        <Link to ='/admin'>
                        <button type="button" className="btn btn-info mt-3 mx-3">Admin</button>
                        </Link>
                        
                        </>
                        :
                        null
                } 

                

                <h2 className="text-center mt-4">Account</h2>
                <div className="d-flex justify-content-center ">
                    <div className="form-group shadow px-5 py-3 my-4 bg-white rounded">
                        <ul className="nav nav-tabs">
                            <li className="nav-item">
                                <a className="nav-link active" data-bs-toggle="tab" href="#account">Infos</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" data-bs-toggle="tab" href="#password" onClick={() => setEdit(false)}>Password</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" data-bs-toggle="tab" href="#security">Logout</a>
                            </li>
                        </ul>
                        <div className='tab-content'>
                            {<div className="tab-pane fade active show" id="account">
                                <AccountForm edit={setEdit} dataEdit={edit} />
                            </div>}


                            {/* PASSWORD FORM */}

                            <div className="tab-pane fade" id="password">
                                <fieldset id="password">
                                    <label className="form-label mt-4">Old password</label>
                                    <input name='oldPassword' onChange={handleChange} className="form-control" type="password" />
                                    <label className="form-label mt-4" htmlFor="readOnlyInput">New Password</label>
                                    <input name='password' onChange={handleChange} className="form-control" type="password" placeholder="new password" />
                                </fieldset>
                                <div className="d-flex justify-content-center pt-3 mt-3">
                                    <button onClick={handleSubmit} type="submit" className="btn btn-outline-primary">Submit</button>
                                </div>



                            </div>

                            <div className="tab-pane fade" id="security">
                                <div className="d-flex justify-content-center pt-3 mt-3">
                                    <button onClick={() => dispatch(logout())} className="btn btn-outline-danger">Se déconnecter</button>
                                </div>
                                <div className="d-flex justify-content-center pt-3 mt-3">
                                    <button onClick={() => delUser(Number(user?.id))} className="btn btn-danger">Supprimer le compte</button>
                                </div>




                            </div>




                        </div>
                    </div>
                </div>

            </div>
        )
    }
    return (
        <Redirect to='/login' />
    )
}