import { useParams } from "react-router-dom";
import { RouterParams } from "../App";
import { Commentaires } from "../components/ReadComments";
import { PostComments } from "../components/PostComments";
import { Rating } from "../components/Rating";
// import { Rating } from "../components/Rating";
import { useGetProduitByIdQuery } from "../shared/Product-api";

export function OneProduit() {

    const { id } = useParams<RouterParams>();

    const { data, isLoading, isError } = useGetProduitByIdQuery(Number(id));


    if (isError) {
        return <p>Une erreur est survenue, veuillez recharger la page</p>
    }

    return (
        <div className="container">

            <div className="row mt-5 justify-content">

                {!isLoading && data ?
                    <>
                        <div className="col-12 col-md-6">
                            <img src={data.picture} style={{height:'25em'}} className="img img-fluid" alt={data.name} />
                        </div>

                        <div className="col-12 col-md-6 mt-md-0 mt-4">
                            <h5 className="name">{data?.name}</h5>
                            <p className="category">Category: {data?.category}</p>
                            <p className="price">Price: {data?.price}€</p>
                            <Rating />
                            <div>Description :</div>
                            <p className='bg-comments py-2 rounded text-black px-2'>{data?.description}</p>

                        </div>
                        <div className='container mt-5'>
                            <div className='row justify-content-center'>
                                <div className='col-md-8 col-10'>
                                    <h4 className='text-center titles'>Avis </h4>
                                {data.comments && <Commentaires comments={data.comments} />}
                                <PostComments product={data}/>
                                </div>
                            </div>
                        </div>

                    </>
                    : <p>Chargement en cours...</p>}
            </div>
        </div>

    )
}

