import { useParams } from "react-router"
import { RouterParams } from "../App"
import { CardProduit } from "../components/CardProduct"
import { useGetAllProduitQuery } from "../shared/Product-api"

export const SearchPage = () => {
    let { id } = useParams<RouterParams>()
    const { data, isLoading, isError, } = useGetAllProduitQuery(id);
    
    
    return (
        <div className="container">
            <div className="row mt-2 justify-content-center">
                <h3 className='text-secondary text-center my-2'>"{id}"</h3>
                {(isLoading && !isError ) && <p>Chargement en cours..</p>}
                {data && !isError ? data.map(item =>
                    <div key={item.id} className="col-12 col-md-4 mt-5">
                        <CardProduit produit={item} />
                    </div>
                ): <h2 className='text-center mt-5 rounded w-auto'>Aucun article ne correspond à votre recherche</h2>}
            </div>
        </div>
    )
}