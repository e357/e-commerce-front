

import { CardProduit } from "../components/CardProduct";
import { useGetAllProduitQuery } from "../shared/Product-api";


export function Produits() {

    const { data, isLoading, isError } = useGetAllProduitQuery();

    if (isLoading) {
        return <p>Loading...</p>
    }

    if (isError) {
        return <p>Une erreur est survenue, veuillez recharger la page</p>
    }

    return (
        <div className="container">
            <div className="row mt-2">

                {data?.map(item =>
                    <div key={item.id} className="col-12 col-md-4 my-1">
                        <CardProduit produit={item} />
                    </div>
                )}

            </div>
        </div>
    )
}