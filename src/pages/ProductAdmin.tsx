import { QueryStatus } from "@reduxjs/toolkit/dist/query";
import { LegacyRef, useEffect, useRef, useState } from "react";
import { ProductAdminList } from "../components/ProductAdminList";
import { Toast } from "../components/Toast";
import { Produit } from "../Entities";
import { useGetAllProduitQuery, usePostProductMutation } from "../shared/Product-api";

export function ProductAdmin() {
    const { data } = useGetAllProduitQuery();
    const [postProduct, productQuery] = usePostProductMutation();
    const [form, setForm] = useState<Produit>({} as Produit

    );
    const formref: LegacyRef<any> = useRef()

    const handleFile = async (event: React.FormEvent<EventTarget>) => {
        let target = event.target as HTMLInputElement;
        let name = target.name;
        let value = target.files?.item(0);
        let change = { ...form, [name]: value }

        setForm(change)
    }



    const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
        try {

            event.preventDefault()

            const formData = new FormData(formref.current);

            await postProduct(formData).unwrap()

        } catch (error: any) {
            console.log(error.data);
        }
    }

    const handleChange = (event: React.FormEvent<EventTarget>) => {
        let target = event.target as HTMLInputElement;
        let name = target.name;
        let value = target.value
        let change = { ...form, [name]: value }

        setForm(change)
    }








    return (
        <div className="container" >
            {productQuery.status === QueryStatus.fulfilled && <Toast sentence={'Le produit a été ajouté !'}/>}

            <h1 className="mt-5 text-dark">Gestion des produits</h1>

            <button data-bs-toggle="modal" data-bs-target="#exampleModal" type="button" className="btn btn-outline-success"><i className="bi bi-plus-square-fill"></i> Add</button>

            <table className="table table-hover">

                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Picture</th>
                        <th scope="col">Name</th>
                        <th scope="col">Category</th>
                        <th scope="col">Price</th>
                        <th scope="col">Active</th>
                        <th scope="col">Description</th>
                        <th scope="col">Edit</th>

                    </tr>
                </thead>


                {data?.map(item =>
                    <ProductAdminList key={item.id}
                        produit={item}

                    />

                )}


            </table>


            <div className="modal fade" id="exampleModal" tabIndex={-1} aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>

                        <div className="modal-body">
                            <form ref={formref} onSubmit={handleSubmit} >
                                <div className="form-group row">
                                    <label htmlFor="text" className="col-sm-2 col-form-label">Name</label>
                                    <input onChange={handleChange} name="name" type="text" className="form-control border-2" />
                                    <label htmlFor="text" className="col-sm-2 col-form-label">Category</label>
                                    <input onChange={handleChange} name="category" type="text" className="form-control border-2" />
                                    <label htmlFor="text" className="col-sm-2 col-form-label">Price</label>
                                    <input onChange={handleChange} name="price" type="text" className="form-control border-2" />


                                    <label htmlFor="floatingTextarea" className="mt-3">Description</label>
                                    <textarea onChange={handleChange} name="description" className="form-control" placeholder="Describe your product" id="floatingTextarea"></textarea>


                                    <div className="form-group">
                                        <label htmlFor="formFile" className="form-label mt-4">Photo</label>
                                        <input onChange={handleFile} name="picture" className="form-control" type="file" id="formFile" />
                                    </div>
                                    {/* <label className="form-check-label col-sm-2 col-form-label mt-3" htmlFor="flexSwitchCheckDefault">Stock</label> */}
                                    {/* <div className="form-check form-switch mx-3 ">
                    
                    <input className="form-check-input" type="checkbox" id="flexSwitchCheckDefault" />
                  </div> */}
                                    <label htmlFor="text" className="col-sm-2 col-form-label">Stock</label>
                                    <input onChange={handleChange} name="stock" type="text" className="form-control border-2" />
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                    <button type="submit" className="btn btn-primary" data-bs-dismiss="modal">Add product</button>
                                </div>
                            </form>
                        </div>


                    </div>
                </div>
            </div>
        </div >


    )
}

//bouton add/edit/delete
//overflow 
// barre de recherche