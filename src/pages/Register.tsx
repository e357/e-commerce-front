import { RegisterForm } from "../components/RegisterForm";

export function Register() {
    return (
        <div className="register">
            <h2 className="text-center mt-4">Register</h2>
            <RegisterForm />
        </div>
    )
}