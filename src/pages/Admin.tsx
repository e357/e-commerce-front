import { Link } from "react-router-dom";

export function Admin() {
    return (
    
            <div className="container align-items-center">
                <div className="row justify-content-around d-flex">
                <div className="col-3 card text-white bg-primary mt-5 align-items-center" >
                        <div className="card-body text-center mt-5">
                            <h2 className="card-title">Products</h2>
                        </div>
                        <Link to='/admin/produits' className="btn btn-outline-light mb-3">
                        Gérer
                        </Link>
                        
                    </div>
               

                </div>
            </div>
 
            )
}