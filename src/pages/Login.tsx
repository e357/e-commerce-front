import { LoginForm } from "../components/LoginForm";


export function Login() {

    return (
        <div className="login">
            <h2 className="text-center mt-4">Login</h2>
            <LoginForm />
        </div>

    )
}