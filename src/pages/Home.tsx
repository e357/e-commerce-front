import "./Home.css"
import { useGetLast3ProductsQuery } from "../shared/Product-api"
import { CardProduit } from "../components/CardProduct"

export function Home() {
    const { data, isLoading, isError } = useGetLast3ProductsQuery()
    
    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-12 ">
                    <div className="picture img-fluid my-3">
                        <h1 className="pt-5">Bienvenue</h1>
                    </div>
                </div>
                {isError ? <p>Aucun produit n'est disponible pour le moment</p>
                    : !isLoading && data ? data.map((value, index) => {
                        return <div key={value.id} className='col-12 my-1 col-md-4'>
                            <CardProduit produit={value} />
                        </div>
                    }) : <p>Chargement en cours...</p>}
            </div>
        </div>


    )
}