import { useState } from "react";
import Avatar from "react-avatar";
import { Order } from "../components/Order";
import { useDeleteCartMutation, useMinusCartMutation, usePostCartMutation } from "../shared/User-api";
import { useAppSelector } from "../stores/hooks";




export function Panier() {
    const user = useAppSelector(state => state.auth.user);
    const [order, setOrder] = useState(false)
    let [patchLine] = usePostCartMutation()
    let [minusLine] = useMinusCartMutation()
    let [deleteLine] = useDeleteCartMutation()



    return (
        user?.panier?.length ? <div className="d-flex justify-content-center overflow-auto">
            <div className='col-12 col-md-6 mt-md-5 mt-3'>
                {!order ? <> <ul className='list-group'>
                    {user?.panier?.map(line => {
                        return <li key={line.id} className='mt-1 list-group-item d-flex justify-content-md-between rounded justify-content-evenly align-items-center flex-wrap'>
                            <div className='col-6 col-md-2'>
                                <Avatar name={line.produit.name} color='black' size='100' src={line.produit.thumbnailPic} />
                            </div>
                            <div className='col-6 col-md-4'>
                                <h5 className='text-md-center text-end'>{line.produit.name}</h5>
                                <h5 className='text-md-center text-end'>{line.price}€</h5>
                            </div>
                            <div className='col-10 col-md-4 mt-3 mt-md-0'>
                                <button onClick={() => minusLine(line.id)} className='btn btn-sm btn-primary'>-</button>
                                <span className='mx-3'>{line.quantity}</span>
                                <button onClick={() => patchLine(line.produit.id)} className='btn btn-sm btn-primary'>+</button>
                            </div>
                            <div className='col-2 col-md-1 mt-3 mt-md-0'>
                                <button onClick={() => deleteLine(line.id)} className='btn btn-sm btn-danger'>X</button>
                            </div>
                        </li>
                    })}
                </ul>
                    <button className='btn btn-primary col-6 col-md-4 mt-5 offset-6 offset-md-8 rounded' onClick={() => setOrder(true)}>Passer la commande</button></> : <Order />}
            </div>
        </div> : <h4 className="m-4 text-center">Aucun article dans le panier</h4>
    )
}