import { Home } from "./pages/Home";
import "bootswatch/dist/lux/bootstrap.min.css";
import './App.css';
import { Switch, BrowserRouter, Route } from 'react-router-dom';
import { Login } from "./pages/Login";
import { Register } from "./pages/Register";
import { Nav } from "./components/Nav";
import { Account } from "./pages/Account";
import { PrivateRoute } from "./utils/PrivateRoute";
import { useGetThisUserQuery } from "./shared/User-api";
import { OneProduit } from "./pages/ProductById";
import { Produits } from "./pages/Produits";
import { Admin } from "./pages/Admin";
import { AdminRoute } from "./utils/AdminRoute";
import { ProductAdmin } from "./pages/ProductAdmin";
import { SearchPage } from "./pages/Search";
import { Panier } from "./pages/Panier";

export interface RouterParams {
    id: string
}
function App() {
    const token = useGetThisUserQuery(undefined, { skip: localStorage.getItem('token') === null })
    if (token.isError) {
        localStorage.removeItem('token')
    }

    return (
        <>
            <BrowserRouter>
                <Nav />
                <Switch>
                    <Route exact path='/'>
                        <Home />
                    </Route>
                    <Route path='/login'>
                        <Login />
                    </Route>
                    <Route path='/register'>
                        <Register />
                    </Route>
                    <Route path='/search/:id'>
                        <SearchPage/>
                    </Route>
                    <PrivateRoute path='/account'>
                        <Account />
                    </PrivateRoute>
                    <Route path='/product/:id'>
                        <OneProduit />
                    </Route>
                    <Route path='/products'>
                        <Produits />
                    </Route>
                    <PrivateRoute path='/cart'>
                        <Panier/>
                    </PrivateRoute>


                    <AdminRoute exact path='/admin'>
                        <Admin />
                    </AdminRoute>

                    <AdminRoute path='/admin/produits'>
                        <ProductAdmin />
                    </AdminRoute>


                </Switch>
            </BrowserRouter>

        </>
    );
}

export default App;
